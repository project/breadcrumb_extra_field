# Breadcrumb Extra Field

Breadcrumb Extra Field allows you to print breadcrumb between fields.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/breadcrumb_extra_field).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/breadcrumb_extra_field).


## Table of contents

- Requirements
- Installation
- Configuration
- Important 
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. In order to enable the extra field into an entity, is needed to configure
   the module administration form:
   URL to configuration form:
   `/admin/config/system/breadcrumb-extra-field`

1. "Administer breadcrumb extra field" permission is used to ensure the
   correct access. Be sure it's correctly configured in People >> Permissions.

1. After configuration just clear cache and set the extra field visibility into
   the desired view mode.


## Important

- Limited to these entity types:
  node, user, taxonomy term, comment and bean.
  If you need to implement into other entity please create a feature request
  issue.


## Maintainers

- CRZDEV - [CRZDEV](https://www.drupal.org/u/crzdev)
